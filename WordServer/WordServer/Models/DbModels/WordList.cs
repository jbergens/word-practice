﻿using System;
using System.Collections.Generic;

namespace WordServer.Models.DbModels
{
    public class WordList
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime CreatedAt { get; set; }
        public string LangCode { get; set; }
        public string ForeignLangCode { get; set; }

        public List<Word> Words { get; set; }
    }
}