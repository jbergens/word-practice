﻿using System;
using System.Data.Entity;

namespace WordServer.Models.DbModels
{
    public class WordContext : DbContext
    {
        public WordContext() : base("WordContext") {}

        public DbSet<WordList> WordLists { get; set; }
        public DbSet<Word> Words { get; set; }
    }
}