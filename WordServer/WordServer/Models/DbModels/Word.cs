﻿namespace WordServer.Models.DbModels
{
    public class Word
    {
        public int Id { get; set; }
        public int WordListId { get; set; }
        public string Text { get; set; }
        public string Translation { get; set; }
        public string Explanation { get; set; }
        public string Synonyms { get; set; }

        public virtual WordList OwnerList { get; set; }
    }
}