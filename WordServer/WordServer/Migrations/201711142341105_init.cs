namespace WordServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WordLists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        LangCode = c.String(),
                        ForeignLangCode = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Words",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WordListId = c.Int(nullable: false),
                        Text = c.String(),
                        Translation = c.String(),
                        Explanation = c.String(),
                        Synonyms = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WordLists", t => t.WordListId, cascadeDelete: true)
                .Index(t => t.WordListId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Words", "WordListId", "dbo.WordLists");
            DropIndex("dbo.Words", new[] { "WordListId" });
            DropTable("dbo.Words");
            DropTable("dbo.WordLists");
        }
    }
}
