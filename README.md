# README #

This is a small training application using a .NET backend and a React frontend.

### Status ###
The code is not finished yet and does not work. It probably don't even compile yet.

### What is this repository for? ###

* Might be used to practice words in a foreign language 
* As a small example of Entity Framework and React. The code will NOT be production quality.

### How do I get set up? ###

* Compile the backend with Visual Studio or the csc compiler
* Build the frontend using npm scripts in package.json
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

This repo is mostly meant to use as a training example. I might continue with the code later to make it useful for my
own purposes but I will probably not accept many pull-requests unless they corresponds closely with my requirements.

